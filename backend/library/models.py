from django.db import models

class Author(models.Model):
    name = models.CharField('ФИО', max_length=50)
    description = models.TextField('Об авторе', max_length=1000,
        null=True, 
        blank=True)
    date_of_birth = models.DateField('Дата рождения', null=True, blank=True)
    date_of_death = models.DateField('Дата смерти', null=True, blank=True)
    like_counter = models.PositiveSmallIntegerField('Кол-во лайков', default=0)

    class Meta:
        ordering = ['name']
        verbose_name = 'Автор'
        verbose_name_plural = 'Авторы'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('author', args=[str(self.id)])

class Genre(models.Model):
    name = models.CharField('Название', max_length=100)
    description = models.TextField('Описание', max_length=1000,
        null=True, 
        blank=True)
    image = models.ImageField(
        null=True, 
        blank=True)

    class Meta:
        ordering = ['name']
        verbose_name = 'Жанр'
        verbose_name_plural = 'Жанры'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('genre', args=[str(self.id)])


class Book(models.Model):
    name = models.CharField('Название', max_length=100)
    description = models.TextField('Описание', max_length=1000,
        null=True, 
        blank=True)
    like_counter = models.PositiveSmallIntegerField('Кол-во лайков', default=0)
    genre = models.ManyToManyField(Genre, help_text='Выберите жанры книги')
    author = models.ManyToManyField(Author, help_text='Выберите авторов книги')

    class Meta:
        ordering = ['name']
        verbose_name = 'Книга'
        verbose_name_plural = 'Книги'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('book', args=[str(self.id)])
